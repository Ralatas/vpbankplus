import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppComponent } from './app.component';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HTTP_INTERCEPTORS } from '@angular/common/http';
import { ApiService } from './services/api.service';
import { LocalStorageService } from './services/storage.service';
import { LoginComponent } from './pages/login/login.component';
import { AppRoutingModule } from './app-routing.module';
import { Page404Component } from './pages/page404/page404.component';
import { LoginotpComponent } from './pages/loginotp/loginotp.component';
import { FirsttimepassComponent } from './pages/firsttimepass/firsttimepass.component';
import { SecretQuestionsComponent } from './pages/secret-questions/secret-questions.component';
import { TranslateService } from './services/translate.service';
import { TranslatePipe} from './pipes/translate.pipe';
import { ModalComponent } from './components/modal/modal.component';
import { LocationStrategy, PathLocationStrategy } from '@angular/common';
import { OtpComponent } from './pages/otp/otp.component';
import { TermsComponent} from './pages/terms/terms.component';
import { OtpEwalletComponent} from './pages/otp-ewallet/otp-ewallet.component';
import { WalletComponent } from './pages/wallet/wallet.component';
import { HeaderInterceptor } from './services/api-interceptor.service';
import { CookiesService } from './services/cookies.service';


@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    Page404Component,
    LoginotpComponent,
    TermsComponent,
    OtpComponent,
    OtpEwalletComponent,
    FirsttimepassComponent,
    SecretQuestionsComponent,
    TranslatePipe,
    ModalComponent,
    WalletComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpClientModule,
    AppRoutingModule,
    ReactiveFormsModule,
  ],
  providers: [
      TranslateService,
      ApiService,
      CookiesService,
      LocalStorageService,
      { provide: LocationStrategy, useClass: PathLocationStrategy },
      {
          provide: HTTP_INTERCEPTORS,
          useClass: HeaderInterceptor,
          multi: true
      },
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
