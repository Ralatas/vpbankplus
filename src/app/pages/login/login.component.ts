import {Component, OnInit} from '@angular/core';
import {ActivatedRoute, Params, Router} from '@angular/router';
import {TranslateService} from '../../services/translate.service';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {ApiService} from '../../services/api.service';
import * as PARAMS from '../../../assets/configs/config.json';
import {Observable} from 'rxjs/Observable';
import 'rxjs/add/operator/map';

@Component({
  selector: 'app-login-do',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  constructor(private route: ActivatedRoute,
              private router: Router,
              private translateService: TranslateService,
              private api: ApiService
  ) {}

  public hiddenTime: any;
  public data: any;
  public authForm: FormGroup;
  public pageIcons: any;
  public pageColors: any;
  public app: string;
  public loginElements: any;
  ngOnInit() {
    this.route.queryParams.subscribe(
        (params: Params) => {
          this.data = params;
          if (this.data.sp && (<any>PARAMS).hasOwnProperty(this.data.sp)) {
            this.app = this.data.sp;
          } else {
            this.app = 'default';
          }
          this.hiddenTime = (<any>PARAMS)[this.app].PopLoginMessageHiddenTime;
          this.loginElements = (<any>PARAMS)[this.app].loginPageElements;
          this.pageIcons = (<any>PARAMS)[this.app].loginPageIcons;
          this.pageColors = (<any>PARAMS)[this.app].pageStyle.login;
          this.translateService.selectLanguage(this.data.lang || 'ENG', this.app);
          this.initForm();
        }
    );
  }

  private initForm() {
    this.authForm = new FormGroup({
      'password': new FormControl(null, [Validators.required, Validators.pattern(/^[\S]{5,30}$/)]),
      'login': new FormControl(this.data.default_username || null, [Validators.required, Validators.pattern(/[a-zA-Z0-9._-|//]{3,30}$/)]),
    });
    if (!this.loginElements.loginField) {
      this.authForm.get('login').clearValidators();
      this.authForm.get('login').updateValueAndValidity();
    }
  }
  onCommanForApp(command) {
    this.api.CommandForApp((<any>PARAMS)[this.app].api_url_commands.login[command], this.app, this.data.sessionDataKey);
  }
  onSubmit() {
    if (this.authForm.valid) {
      this.api.onCommonauth(this.authForm.value, this.data.sessionDataKey, this.app)
          .subscribe(
        (response) => {
          if (response.status !== 200) {
            this.api.CommandForApp((<any>PARAMS)[this.app].api_url_commands.login['ERROR'], this.app, this.data.sessionDataKey);
          } else {
            location.href = response.headers.get('Location');
          }
        },
          (error) => {
            this.api.CommandForApp((<any>PARAMS)[this.app].api_url_commands.login['ERROR'], this.app, this.data.sessionDataKey);
        });
    } else {
      this.authForm.controls.password.markAsTouched();
      this.authForm.controls.login.markAsTouched();
    }
  }
}
