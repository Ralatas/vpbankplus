import {Component, OnDestroy, OnInit} from '@angular/core';
import {FormControl, FormGroup, Validators} from "@angular/forms";
import {ActivatedRoute, Params} from "@angular/router";
import {ApiService} from "../../services/api.service";
import {TranslateService} from "../../services/translate.service";
import * as PARAMS from '../../../assets/configs/config.json';

@Component({
  selector: 'app-loginotp',
  templateUrl: './loginotp.component.html',
  styleUrls: ['./loginotp.component.scss']
})
export class LoginotpComponent implements OnInit , OnDestroy {

  constructor(private route: ActivatedRoute, private translateService: TranslateService,  private api: ApiService) {
  }
  public data: any;
  public OPTForm: FormGroup;
  public hiddenTime: any;
  public pageColors: any;
  public timeForEnterCode: any;
  public timer: any;
  public app: string;
  ngOnInit() {
    this.route.queryParams.subscribe(
        (params: Params) => {
          this.data = params;
          if (this.data.sp && (<any>PARAMS).hasOwnProperty(this.data.sp)) {
            this.app = this.data.sp;
          } else {
            this.app = 'default';
          }
          this.hiddenTime = (<any>PARAMS)[this.app].PopLoginOTPMessageHiddenTime;
          this.pageColors = (<any>PARAMS)[this.app].pageStyle.loginOPT;
          this.timeForEnterCode = (<any>PARAMS)[this.app].OPT_TIME;
          this.translateService.selectLanguage(this.data.lang || 'ENG', this.app);
          this.initForm();
        }
    );
    this.timer = setInterval(() => {
      if (this.timeForEnterCode > 0) {
        this.timeForEnterCode -= 1;
      } else {
        alert('DO something');
        clearInterval(this.timer);
      }
    },  1000);
  }

  private initForm() {
    this.OPTForm = new FormGroup({
      'password': new FormControl(null, [Validators.required, Validators.pattern(/[0-9]{6}$/)]),
    });
  }
  ngOnDestroy() {
    clearInterval(this.timer);
  }
  onSubmit() {
    if (this.OPTForm.valid) {
      this.api.onLoginotpConsent(this.OPTForm.value, this.data.sessionDataKey, this.app).subscribe(
          (response) => {
            if (response.status !== 200) {
              this.api.CommandForApp((<any>PARAMS)[this.app].api_url_commands.loginOPT['ERROR'], this.app, this.data.sessionDataKey);
            } else {
              location.href = response.headers.get('Location');
            }
          },
          (error) => {
            this.api.CommandForApp((<any>PARAMS)[this.app].api_url_commands.loginOPT['ERROR'], this.app, this.data.sessionDataKey);
          });
    } else {
      this.OPTForm.controls.password.markAsTouched();
    }
  }
  onResend() {
    this.api.onLoginotpResend(this.data.sessionDataKey, this.app).subscribe(
        (response) => {
          if (response.status !== 200) {
            this.api.CommandForApp((<any>PARAMS)[this.app].api_url_commands.loginOPT['ERROR'], this.app, this.data.sessionDataKey);
          } else {
            location.href = response.headers.get('Location');
          }
        },
        (error) => {
          this.api.CommandForApp((<any>PARAMS)[this.app].api_url_commands.loginOPT['ERROR'], this.app, this.data.sessionDataKey);
        });
  }
  _keyPress(event: any) {
    const pattern = /^[0-9]*$/;
    let inputChar = String.fromCharCode(event.charCode);
    if (inputChar === event.key) {
      if (!pattern.test(inputChar)) {
        event.preventDefault();
      }
    }
  }
  onCommanForApp(command) {
    this.api.CommandForApp((<any>PARAMS)[this.app].api_url_commands.loginOPT[command], this.app, this.data.sessionDataKey);
  }
}
