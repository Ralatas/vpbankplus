import { Component, OnInit } from '@angular/core';
import {ActivatedRoute, Params} from "@angular/router";
import {TranslateService} from "../../services/translate.service";
import {ApiService} from "../../services/api.service";
import {FormControl, FormGroup, Validators} from "@angular/forms";
import * as PARAMS from '../../../assets/configs/config.json';
@Component({
  selector: 'app-firsttimepass',
  templateUrl: './firsttimepass.component.html',
  styleUrls: ['./firsttimepass.component.scss']
})
export class FirsttimepassComponent implements OnInit {

  constructor(private route: ActivatedRoute, private translateService: TranslateService,  private api: ApiService) {
  }
  public data: any;
  public FTPForm: FormGroup;
  public errorPassword = false;
  public hiddenTime: any;
  public app: string;
  ngOnInit() {
    this.route.queryParams.subscribe(
        (params: Params) => {
          this.data = params;
          if (this.data.sp && (<any>PARAMS).hasOwnProperty(this.data.sp)) {
            this.app = this.data.sp;
          } else {
            this.app = 'default';
          }
          this.hiddenTime = (<any>PARAMS)[this.app].PopFTPHiddenTime;
          this.translateService.selectLanguage(this.data.lang || 'ENG', this.app);
          this.initForm();
        }
    );
  }

  private initForm() {
    this.FTPForm = new FormGroup({
      'currentPassword': new FormControl(null, [Validators.required, Validators.pattern(/^[\S]{5,30}$/)]),
      'newPassword': new FormControl(null, [Validators.required, Validators.pattern(/^[\S]{5,30}$/)]),
      'repNewPassword': new FormControl(null, [Validators.required, Validators.pattern(/^[\S]{5,30}$/)]),
    });
  }
  onSubmit() {
    if (this.FTPForm.valid && this.FTPForm.value.newPassword === this.FTPForm.value.repNewPassword) {
      this.api.onFirsttimepass(this.FTPForm.value, this.data.sessionDataKey, this.app).subscribe(
          (response) => {
            if (response.status !== 200) {
              this.api.CommandForApp((<any>PARAMS)[this.app].api_url_commands.firstTimePass['ERROR'], this.app, this.data.sessionDataKey);
            } else {
              location.href = response.headers.get('Location');
            }
          },
          (error) => {
            this.api.CommandForApp((<any>PARAMS)[this.app].api_url_commands.firstTimePass['ERROR'], this.app, this.data.sessionDataKey);
          });
    } else if (this.FTPForm.valid && this.FTPForm.value.newPassword !== this.FTPForm.value.repNewPassword) {
      this.errorPassword = true;
    } else {
      this.FTPForm.controls.currentPassword.markAsTouched();
      this.FTPForm.controls.newPassword.markAsTouched();
      this.FTPForm.controls.repNewPassword.markAsTouched();
    }
  }
  onCloseModal() {
    this.errorPassword = false;
  }
}
