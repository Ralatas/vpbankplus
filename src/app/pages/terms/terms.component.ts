import {Component, OnInit} from '@angular/core';
import {ActivatedRoute, Params, Router} from '@angular/router';
import {ApiService} from '../../services/api.service';
import {TranslateService} from '../../services/translate.service';
import * as PARAMS from '../../../assets/configs/config.json';

@Component({
  selector: 'app-terms',
  templateUrl: './terms.component.html',
  styleUrls: ['./terms.component.scss']
})
export class TermsComponent implements OnInit {

  constructor(private route: ActivatedRoute, private router: Router, private translateService: TranslateService, private api: ApiService) {
  }

  public data: any;
  public pageColors: any;
  public app: string;
  public hiddenTime: any;
  ngOnInit() {
    this.route.queryParams.subscribe(
      (params: Params) => {
        this.data = params;
        if (this.data.sp && (<any>PARAMS).hasOwnProperty(this.data.sp)) {
          this.app = this.data.sp;
        } else {
          this.app = 'default';
        }
        this.hiddenTime = (<any>PARAMS)[this.app].PopFTPHiddenTime;
        this.pageColors = (<any>PARAMS)[this.app].pageStyle.terms;
        this.translateService.selectLanguage(this.data.lang || 'ENG', this.app);
      }
    );
  }

  onTerms(checkboxValue) {
    if (checkboxValue.checked) {
      this.api.onTerms(this.data.sessionDataKey, this.app).subscribe(
        (response) => {
          if (response.status !== 200) {
            this.api.CommandForApp((<any>PARAMS)[this.app].api_url_commands.login['ERROR'], this.app, this.data.sessionDataKey);
          } else {
            location.href = response.headers.get('Location');
          }
        },
        (error) => {
          this.api.CommandForApp((<any>PARAMS)[this.app].api_url_commands.login['ERROR'], this.app, this.data.sessionDataKey);
        });
    }
  }
}
