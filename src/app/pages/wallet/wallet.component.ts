import { Component, OnInit } from '@angular/core';
import {ActivatedRoute, Params, Router} from '@angular/router';
import * as PARAMS from '../../../assets/configs/config.json';
import {TranslateService} from '../../services/translate.service';
import {ApiService} from '../../services/api.service';

@Component({
  selector: 'app-wallet',
  templateUrl: './wallet.component.html',
  styleUrls: ['./wallet.component.scss']
})
export class WalletComponent implements OnInit {
  public data: any;
  public pageColors: any;
  public pageIcons: any;
  public app: string;
  public hiddenTime: any;
  public accounts: any[];

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private translateService: TranslateService,
    private api: ApiService,
    ) { }

  ngOnInit() {
    this.route.queryParams.subscribe(
      (params: Params) => {
        this.data = params;
        if (this.data.sp && (<any>PARAMS).hasOwnProperty(this.data.sp)) {
          this.app = this.data.sp;
        } else {
          this.app = 'default';
        }
        this.api.onGetAccounts(this.app, this.data.sessionDataKey)
          .subscribe((accounts) => this.accounts = accounts);
        this.hiddenTime = (<any>PARAMS)[this.app].PopWalletLinkHiddenTime;
        this.translateService.selectLanguage(this.data.lang || 'ENG', this.app);
        this.pageColors = (<any>PARAMS)[this.app].pageStyle.wallet;
        this.pageIcons = (<any>PARAMS)[this.app].loginPageIcons;
      }
    );
  }

  onSubmit(account) {
    this.api.onCreateWalletLink(this.app, this.data.sessionDataKey, account)
      .subscribe(
        (response) => {
          if (response.status === 200) {
            this.api.onWalletOtp(this.app, this.data.sessionDataKey, this.data.lang || 'ENG')
              .subscribe(
                (wRes) => {
                  if (wRes.status !== 200) {
                    this.api.CommandForApp((<any>PARAMS)[this.app].api_url_commands.wallet['ERROR'], this.app, this.data.sessionDataKey);
                  } else {
                    location.href = response.headers.get('Location');
                  }},
                () => {
                  this.api.CommandForApp((<any>PARAMS)[this.app].api_url_commands.wallet['ERROR'], this.app, this.data.sessionDataKey);
                });
          }
        },
        (error) => {
          if (error.status === 400 || error.status === 404) {
            const currentParams = this.route.snapshot.queryParams;
            this.router.navigate([this.route.url], {
              queryParams: {...currentParams, 'popupMsg': error.error},
            });
          } else {
            this.api.CommandForApp((<any>PARAMS)[this.app].api_url_commands.wallet['ERROR'], this.app, this.data.sessionDataKey);
          }
        },
        );
  }

  onCommanForApp(command) {
    this.api.CommandForApp((<any>PARAMS)[this.app].api_url_commands.wallet[command], this.app, this.data.sessionDataKey);
  }
}
