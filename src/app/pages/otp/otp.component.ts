import {Component, OnDestroy, OnInit} from '@angular/core';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {ActivatedRoute, Params, Router} from '@angular/router';
import {ApiService} from '../../services/api.service';
import {TranslateService} from '../../services/translate.service';
import * as PARAMS from '../../../assets/configs/config.json';
@Component({
  selector: 'app-otp',
  templateUrl: './otp.component.html',
  styleUrls: ['./otp.component.scss']
})
export class OtpComponent implements OnInit, OnDestroy {

  constructor(private route: ActivatedRoute, private router: Router, private translateService: TranslateService,  private api: ApiService) {
  }
  public data: any;
  public hiddenTime: any;
  public pageColors: any;
  public OPTForm: FormGroup;
  public timer: any;
  public timeForEnterCode: any;
  public app: string;
  public blockSend = false;


  public listLogins = [];
  ngOnInit() {
    this.route.queryParams.subscribe(
        (params: Params) => {
          this.data = params;
          if (this.data.sp && (<any>PARAMS).hasOwnProperty(this.data.sp)) {
            this.app = this.data.sp;
          } else {
            this.app = 'default';
          }
          this.pageColors = (<any>PARAMS)[this.app].pageStyle.otp;
          this.translateService.selectLanguage(this.data.lang || 'ENG', this.app);
          this.hiddenTime = (<any>PARAMS)[this.app].PopOPTHiddenTime;
          this.timeForEnterCode = +this.data.expireTime || 180;
          this.initForm();
        }
    );
    this.timer = setInterval(() => {
      if (this.timeForEnterCode > 0) {
        this.timeForEnterCode -= 1;
      } else {
        this.blockSend = true;
        clearInterval(this.timer);
      }
    },  1000);
  }
  ngOnDestroy() {
    clearInterval(this.timer);
  }
  private initForm() {
    this.OPTForm = new FormGroup({
      'password': new FormControl(null, [Validators.required, Validators.pattern(/[0-9]{6}$/)]),
    });
  }
  onSubmit() {
    if (this.OPTForm.valid) {
      this.api.onConfirmOTP(this.OPTForm.value, this.data.sessionDataKey, this.app).subscribe(
          (response) => {
            if (response.status === 200) {
              if (this.data.login === 'true') {
                this.api.onDeviceOTP(this.data.sessionDataKey, this.app).subscribe(
                    (enterRes) => {
                      if (enterRes.status !== 200) {
                        this.api.CommandForApp((<any>PARAMS)[this.app].api_url_commands.otp['ERROR'], this.app, this.data.sessionDataKey);
                      } else {
                        location.href = response.headers.get('Location');
                      }},
                    (error) => {
                      this.api.CommandForApp((<any>PARAMS)[this.app].api_url_commands.otp['ERROR'], this.app, this.data.sessionDataKey);
                    });
              } else {
                this.api.CommandForApp((<any>PARAMS)[this.app].api_url_commands.otp['CONFIRM'], this.app, this.data.sessionDataKey);
              }
            }
          },
          (error) => {
            if (error.status === 400 || error.status === 422) {
              const currentParams = this.route.snapshot.queryParams;
              this.router.navigate([this.route.url], {
                queryParams: {...currentParams, 'popupMsg': error.error},
              });
            } else {
              this.api.CommandForApp((<any>PARAMS)[this.app].api_url_commands.otp['ERROR'], this.app, this.data.sessionDataKey);
            }
          });
    } else {
      this.OPTForm.controls.password.markAsTouched();
    }
  }
  onResend() {
    this.api.onResendOTP(this.data.sessionDataKey, this.data.lang, this.app).subscribe(
        (response) => {
          if (response.status === 200) {
            const currentParams = this.route.snapshot.queryParams;
            this.router.navigate([this.route.url], {
              queryParams: {...currentParams, 'popupMsg': 'RESEND_NEW_PASSWORD'},
            });
          }
        }, (error) => {
          if (error.status === 400) {
            const currentParams = this.route.snapshot.queryParams;
            this.router.navigate([this.route.url], {
              queryParams: {...currentParams, 'popupMsg': error.error},
            });
          } else {
            this.api.CommandForApp((<any>PARAMS)[this.app].api_url_commands.otp['ERROR'], this.app, this.data.sessionDataKey);
          }
        });
  }
  _keyPress(event: any) {
    const pattern = /^[0-9]*$/;
    let inputChar = String.fromCharCode(event.charCode);
    if (inputChar === event.key) {
      if (!pattern.test(inputChar)) {
        event.preventDefault();
      }
    }
  }
  onCommanForApp(command) {
    this.api.CommandForApp((<any>PARAMS)[this.app].api_url_commands.otp[command], this.app, this.data.sessionDataKey);
  }
}
