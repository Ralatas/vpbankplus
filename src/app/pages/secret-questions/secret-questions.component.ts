import { Component, OnInit } from '@angular/core';
import {ActivatedRoute, Params} from '@angular/router';
import {ApiService} from '../../services/api.service';
import {TranslateService} from '../../services/translate.service';
import * as PARAMS from '../../../assets/configs/config.json';
@Component({
  selector: 'app-secret-questions',
  templateUrl: './secret-questions.component.html',
  styleUrls: ['./secret-questions.component.scss']
})
export class SecretQuestionsComponent implements OnInit {

  constructor(private api: ApiService, private route: ActivatedRoute, private translateService: TranslateService) {
  }
  public data: any;
  public answer: string = '';
  public app: string;
  public questions: Array<string> = [
    'MOTHER_NAME', 'PRIMARY_SCHOOL_NAME'
  ];
  public selectQuestion: number;
  public hiddenTime: any;
  ngOnInit() {
    this.route.queryParams.subscribe(
        (params: Params) => {
          this.data = params;
          if (this.data.sp && (<any>PARAMS).hasOwnProperty(this.data.sp)) {
            this.app = this.data.sp;
          } else {
            this.app = 'default';
          }
          this.hiddenTime = (<any>PARAMS)[this.app].PopQuestionsHiddenTime;
          this.translateService.selectLanguage(this.data.lang || 'ENG', this.app);
        }
    );
  }
  onSendQuestion() {
    if (this.answer !== '') {
      this.api.onSecretQuestion(this.questions[this.selectQuestion], this.answer, this.data.sessionDataKey, this.app).subscribe(
          (response) => {
            if (response.status !== 200) {
              this.api.CommandForApp((<any>PARAMS)[this.app].api_url_commands.secretQuestion['ERROR'], this.app, this.data.sessionDataKey);
            } else {
              location.href = response.headers.get('Location');
            }
          },
          (error) => {
            this.api.CommandForApp((<any>PARAMS)[this.app].api_url_commands.secretQuestion['ERROR'], this.app, this.data.sessionDataKey);
          });
    }
  }
  onSelectQuestion(number) {
    this.selectQuestion = number;
    this.answer = '';
  }

}
