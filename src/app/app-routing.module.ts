import {RouterModule, Routes} from '@angular/router';

import {NgModule} from '@angular/core';
import {Page404Component} from './pages/page404/page404.component';
import {LoginComponent} from './pages/login/login.component';
import {LoginotpComponent} from './pages/loginotp/loginotp.component';
import {FirsttimepassComponent} from './pages/firsttimepass/firsttimepass.component';
import {SecretQuestionsComponent} from './pages/secret-questions/secret-questions.component';
import {OtpComponent} from './pages/otp/otp.component';
import {TermsComponent} from './pages/terms/terms.component';
import {OtpEwalletComponent} from './pages/otp-ewallet/otp-ewallet.component';
import {WalletComponent} from './pages/wallet/wallet.component';

const appRoutes: Routes = [
  {path: '', redirectTo: 'login.do', pathMatch: 'full'},
  {path: 'login.do', component: LoginComponent},
  {path: 'terms.do', component: TermsComponent},
  {path: 'otp.do', component: OtpComponent, pathMatch: 'full'},
  {path: 'otp-ewallet.do', component: OtpEwalletComponent},
  {path: 'wallet.do', component: WalletComponent},
  {path: 'loginotp.do', component: LoginotpComponent},
  {path: 'firsttimepass.do', component: FirsttimepassComponent},
  {path: 'setupchallenge.do', component: SecretQuestionsComponent},
  {path: 'openui/login.do', component: LoginComponent},
  {path: 'openui/terms.do', component: TermsComponent},
  {path: 'openui/otp.do', component: OtpComponent, pathMatch: 'full'},
  {path: 'openui/otp-ewallet.do', component: OtpEwalletComponent},
  {path: 'openui/loginotp.do', component: LoginotpComponent},
  {path: 'openui/firsttimepass.do', component: FirsttimepassComponent},
  {path: 'openui/setupchallenge.do', component: SecretQuestionsComponent},
  {path: '**', component: Page404Component}
];

@NgModule({
  imports: [
    RouterModule.forRoot(appRoutes)
  ],
  exports: [RouterModule]
})
export class AppRoutingModule {}
