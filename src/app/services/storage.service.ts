import { Injectable } from '@angular/core';

class MemoryStorageService extends Storage {
  private _storage = {};

  clear() {
    this._storage = {};
  }

  getItem(key: string) {
    return this._storage[key];
  }

  removeItem(key: string) {
    delete this._storage[key];
  }

  setItem(key: string, data: string) {
    this._storage[key] = data;
  }
}

@Injectable()
export class LocalStorageService {
  private _storage = window.sessionStorage || new MemoryStorageService();

  getItem(key: string) {
    return this._storage.getItem(key);
  }

  hasItem(key: string) {
    return !!this._storage.getItem(key);
  }

  removeItem(key: string) {
    this._storage.removeItem(key);
  }

  setItem(key: string, data: string) {
    this._storage.setItem(key, data);
  }
}
