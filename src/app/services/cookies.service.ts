import { Injectable } from '@angular/core';
import { LocalStorageService } from './storage.service';

@Injectable()
export class CookiesService {
  constructor(private storage: LocalStorageService) { }

  private check(cookies: string, name: string) {
    name = encodeURIComponent(name);
    const regexp = new RegExp('(?:^' + name + '|;\\s*' + name + ')=(.*?)(?:;|$)', 'g');
    return regexp.test(cookies);
  }

  cut(cookies: string, name: string) {
    if (this.check(cookies, name)) {
      name = encodeURIComponent(name);
      const regexp = new RegExp('(?:^' + name + '|;\\s*' + name + ')=(.*?)(?:;|$)', 'g');
      const result = regexp.exec(cookies);
      return decodeURIComponent(result[1]);
    } else {
      return '';
    }
  }

  get(name: string) {
    return this.storage.hasItem(name) ?
      this.storage.getItem(name) : '';
  }

  set(cookies: string, name: string) {
      const value = this.cut(cookies, name);
      if (value !== '') {
        this.storage.setItem(name, value);
      }
  }

  remove(name: string) {
    if (this.storage.hasItem(name)) {
      this.storage.removeItem(name);
    }
  }
}
