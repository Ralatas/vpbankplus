import { Injectable } from '@angular/core';

import * as PARAMS from '../../assets/configs/config.json';

const WORDS = (<any>PARAMS);
@Injectable()
export class TranslateService {

    private currentLanguage = 'ENG';
    private listLangs = ['ENG', 'VN', 'RUS'];
    private curentApp = 'default';
    constructor() { }
    translate(str) {
        return WORDS[this.curentApp].TRANSLATIONS[str][this.currentLanguage];
    }
    selectLanguage(language, app) {
        if (this.listLangs.indexOf(language) > -1) {
          this.currentLanguage = language;
        }
        this.curentApp = app;
    }
}