import {Injectable} from "@angular/core";
import {
  HttpClient, HttpErrorResponse, HttpEvent, HttpHandler, HttpInterceptor, HttpParams,
  HttpRequest, HttpResponseBase
} from "@angular/common/http";
import {HttpHeaders} from '@angular/common/http';
import {Observable} from "rxjs/Observable";
import * as PARAMS from '../../assets/configs/config.json';

import 'rxjs/add/observable/throw';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/do';
import {CookiesService} from "./cookies.service";

const API_POST = (<any>PARAMS);


@Injectable()
export class ApiService {
    constructor(private http: HttpClient, private cookies: CookiesService) {}
    onCommonauth(data, key, app): Observable<any> {
      const body = new HttpParams()
          .set('username', data.login)
          .set('sessionDataKey', key)
          .set('password', data.password)
          .set('openui', 'js');
      return this.http.post(API_POST[app].api_url + '/commonauth', body, {
        headers: new HttpHeaders()
            .set('Content-Type', 'application/x-www-form-urlencoded'), observe: 'response'
      });
    }
    onLoginotpConsent(data, key, app): Observable<any> {
      const body = new HttpParams()
          .set('code', data.password)
          .set('sessionDataKey', key)
          .set('openui', 'js');
      return this.http.post(API_POST[app].api_url + '/security/loginotp/consent', body, {
        headers: new HttpHeaders()
            .set('Content-Type', 'application/x-www-form-urlencoded'), observe: 'response'
      });
    }
    onLoginotpResend(key, app): Observable<any> {
      const body = new HttpParams()
          .set('sessionDataKey', key)
          .set('openui', 'js');
      return this.http.put(API_POST[app].api_url + '/security/loginotp/resend', body, {
        headers: new HttpHeaders()
            .set('Content-Type', 'application/x-www-form-urlencoded'), observe: 'response'
      });
    }
    onFirsttimepass(data, key, app): Observable<any> {
      const body = new HttpParams()
          .set('oldpass', data.currentPassword)
          .set('newpass', data.newPassword)
          .set('sessionDataKey', key)
          .set('openui', 'js');
      return this.http.post(API_POST[app].api_url + '/commonauth/firsttimepass', body, {
        headers: new HttpHeaders()
            .set('Content-Type', 'application/x-www-form-urlencoded'), observe: 'response'
      });
    }
    onSecretQuestion(question, answer, key, app): Observable<any> {
      const body = new HttpParams()
          .set('question', question)
          .set('answer', answer)
          .set('sessionDataKey', key)
          .set('openui', 'js');
      return this.http.post(API_POST[app].api_url + '/commonauth/challenge', body, {
        headers: new HttpHeaders()
            .set('Content-Type', 'application/x-www-form-urlencoded'), observe: 'response'
      });
    }
    onGetLogin(app): Observable<any> {
      return this.http.get(API_POST[app].api_url_otp_logins);
    }
    onGetAccounts(app, _request_id?): Observable<any> {
      const headers = new HttpHeaders();
      headers.append('Content-Type', 'application/json');
      const request_id = _request_id ? _request_id : '';
      return this.http.get(`${API_POST[app].api_url_list_accounts}${request_id}`, { headers });
    }
    onCreateWalletLink(app, sessionDataKey, accountNumber): Observable<any> {
      return this.http.post(
        `${API_POST[app].api_url_wallet_create_link}/consent/enrich/${sessionDataKey}`,
        { accountNumber },
        { observe: 'response' },
        );
    }
    onConfirmOTP(data, key, app): Observable<any> {
      const tempObj = {'code': data.password , 'sessionDataKey': key };
      if (data.hasOwnProperty('login')) {
        tempObj['account'] = data.login;
      }
      const body = JSON.stringify(tempObj);
      const headers = new HttpHeaders();
      headers.append('Content-Type', 'application/json');
      return this.http.post(API_POST[app].api_url_otp_confirm, body, { headers , observe: 'response'});
    }
    onDeviceOTP(key, app): Observable<any> {
      const body = new HttpParams()
          .set('sessionDataKey', key)
          .set('openui', 'js');
      return this.http.post(API_POST[app].api_url_otp_device, body, {
        headers: new HttpHeaders()
            .set('Content-Type', 'application/x-www-form-urlencoded'), observe: 'response'
      });
    }
    onWalletOtp(app, requestId, lang): Observable<any> {
      const params = new HttpParams()
        .set('lang', lang);
      const headers = new HttpHeaders()
        .set('IDN-App', app);
      return this.http.get(`${API_POST[app].api_url_wallet_otp}/otp/send/${requestId}`, { params, headers });
    }
    onResendOTP(key , lang, app): Observable<any> {
      const body = JSON.stringify({'lang': lang , 'sessionDataKey': key });
      const headers = new HttpHeaders();
      headers.append('Content-Type', 'application/json');
      return this.http.post(API_POST[app].api_url_otp_resend, body, { headers , observe: 'response'});
    }
    onTerms(key, app): Observable<any> {
        const body = new HttpParams()
            .set('sessionDataKey', key)
            .set('openui', 'js');
        return this.http.post(API_POST[app].api_url_terms, body, {
            headers: new HttpHeaders()
                .set('Content-Type', 'application/x-www-form-urlencoded'), observe: 'response'
        });
    }
    CommandForApp(url, app, key) {
      if (API_POST[app].api_url_commands_type === 'send') {
        fetch(url + '?sessionDataKey=' + key);
      } else {
        location.href = url + '?sessionDataKey=' + key;
      }
    }
}
