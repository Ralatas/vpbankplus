import { Injectable, Injector } from '@angular/core';
import { HttpEvent, HttpHandler, HttpInterceptor, HttpRequest, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { CookiesService } from './cookies.service';

@Injectable()
export class HeaderInterceptor implements HttpInterceptor {
  constructor(private injector: Injector) { }

  intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    const cookies = this.injector.get(CookiesService);
    let requestClone = null;
    // Если есть куки на отправку, добавляем
    if (cookies.get('linkCookie') !== '') {
      requestClone = request.clone({
        headers: request.headers.set('Set-Cookie', `linkCookie=${cookies.get('linkCookie')}`)
      });
      cookies.remove('linkCookie');
    }
    // Если с запросом пришли куки - записываем
    if (request.headers.get('Set-Cookie')) {
      cookies.set(request.headers.get('Set-Cookie'), 'linkCookie');
    }
    return next.handle(requestClone !== null ? requestClone : request);
  }
}
