import {Component, OnInit} from '@angular/core';
import {TranslateService} from './services/translate.service';
import {ActivatedRoute, Params} from "@angular/router";
import * as PARAMS from '../assets/configs/config.json';
@Component({
    selector: 'app-root',
    templateUrl: './app.component.html',
    styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
    constructor(private translateService: TranslateService, private route: ActivatedRoute) { }
    title = 'app';
    public loginParams = {
    default_username: 'testUser',
    lang: 'ENG',
    client_id: 'fQlOnzhkdQqrwE_i0uVsrThf1yMa',
    commonAuthCallerPath: '/oauth2/authorize',
    forceAuth: false,
    passiveAuth: false,
    redirect_uri: 'http://localhost:9999',
    response_type: 'code',
    scope: 'openid vpbp',
    tenantDomain: 'carbon.super',
    sessionDataKey: '729f37be-e5ca-4ecc-8c1f-0ab4b3171a19',
    relyingParty: 'fQlOnzhkdQqrwE_i0uVsrThf1yMa', // qmA2MzwRGSvoI9iUS_lBijF3NKca
    type: 'oidc',
    sp: 'testApp',
    isSaaSApp: false,
    authenticators: 'BasicAuthenticator:LOCAL',
    authFailure: true,
    authFailureMsg: 'login.fail.message'};
    public OPTParams = {
    login: true,
    lang: 'ENG',
    sp: 'testApp',
    expireTime: 120,
    sessionDataKey: '729f37be-e5ca-4ecc-8c1f-0ab4b3171a19',
    };
    public data: any;
    public app: string;
    public background: any;
    selectLanguage(str , app) {
        return this.translateService.selectLanguage(str, app);
    }
    ngOnInit() {
      this.route.queryParams.subscribe(
        (params: Params) => {
          this.data = params;
          if (this.data.sp && (<any>PARAMS).hasOwnProperty(this.data.sp)) {
            this.app = this.data.sp;
          } else {
            this.app = 'default';
          }
          this.background = (<any>PARAMS)[this.app].pageStyle.mainBackground;
        }
      );
    }
}
