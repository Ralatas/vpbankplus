import {Component, Input, OnDestroy, OnInit} from '@angular/core';
import {TranslateService} from "../../services/translate.service";
import {ActivatedRoute, Params} from "@angular/router";
import * as PARAMS from '../../../assets/configs/config.json';
@Component({
  selector: 'app-modal',
  templateUrl: './modal.component.html',
  styleUrls: ['./modal.component.scss']
})
export class ModalComponent implements OnInit, OnDestroy {

  constructor(private route: ActivatedRoute, private translateService: TranslateService) { }
  @Input('modalText') modalText: string;
  @Input('hiddenTime') hiddenTime: number;
  @Input('errorStatus') errorStatus = false;
  public isHiddenModal = false;
  public timeOut: any;
  public data: any;
  public app: string;
  public componentColors: any;
  ngOnInit() {
    this.route.queryParams.subscribe(
        (params: Params) => {
          this.data = params;
          if (this.data.sp && (<any>PARAMS).hasOwnProperty(this.data.sp)) {
            this.app = this.data.sp;
          } else {
            this.app = 'default';
          }
          this.componentColors = (<any>PARAMS)[this.app].pageStyle.modal;
          this.translateService.selectLanguage(this.data.lang || 'ENG', this.app);
        }
    );
    this.timeOut = setTimeout(() => {
      this.onCloseModal();
    }, this.hiddenTime * 1000);
  }
  onCloseModal() {
    this.isHiddenModal = true;
    clearTimeout(this.timeOut);
  }
  ngOnDestroy() {
    clearTimeout(this.timeOut);
  }
}
